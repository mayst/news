const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Page = new mongoose.Schema({
    url: String,
    name: String,
    content: String,
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

Page.plugin(mongoosePaginate);

Page.methods.getPageInfo = function () {
    return { email: this.username, firstName: this.firstName, secondName: this.secondName };
};

module.exports = mongoose.model('Page', Page);
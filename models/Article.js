const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Article = new mongoose.Schema({
    title: {
        type: String,
        unique: true
    },
    content: String,
    thumbnail: {
        type: String,
        default: 'default.png'
    },
    category: [{
        type: String,
        enum: [
            'top-theme',
            'new-lawmaking',
            'foreign-media',
            'market-research',
            'our-interlocutor',
            'ratings',
            'security',
            'gossip',
            'fun-hour',
            'retro-calendar'
        ]
    }],
    watched: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        enum: ['archived', 'published'],
        default: 'archived'
    },
    published_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
});

Article.plugin(mongoosePaginate);

Article.methods.getArticleInfo = function () {
    return { email: this.username, firstName: this.firstName, secondName: this.secondName };
};

module.exports = mongoose.model('Article', Article);
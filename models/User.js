const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const passportLocalMongoose = require('passport-local-mongoose');
const bcrypt = require('bcrypt');

const User = new mongoose.Schema({
    email: String,
    password: String,
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    },
    banned: {
        type: Boolean,
        default: false
    }
});

User.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

User.methods.validPassword = async (password, pass) => {
    try {
        console.log(this.password);
        return await bcrypt.compare(password, pass);
    } catch (err) {
        console.log(err);
    }
};

User.methods.getUserInfo = function () {
    return { email: this.email };
};

module.exports = mongoose.model('User', User);
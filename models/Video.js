const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Video = new mongoose.Schema({
    title: String,
    url: String,
    watched: {
        type: Number,
        default: 0
    },
    category: String,
    status: {
        type: String,
        enum: ['archived', 'published'],
        default: 'archived'
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

Video.plugin(mongoosePaginate);

Video.methods.getArticleInfo = function () {
    return { email: this.username, firstName: this.firstName, secondName: this.secondName };
};

module.exports = mongoose.model('Video', Video);
const express = require('express');
const router = express.Router();
const uploadToContent = require('./../middlewares/imageContent');
const uploadToThumbnail = require('./../middlewares/imageThumbnail');

const UserController = require('./../controllers/UserController');
const AuthController = require('./../controllers/AuthController');
const ArticleController = require('./../controllers/ArticleController');
const PageController = require('./../controllers/PageController');
const NewsController = require('./../controllers/NewsController');
const VideoController = require('./../controllers/VideoController');
const ImageController = require('./../controllers/ImageController');


// Auth
router.post('/registration', UserController.create);

router.post('/login', AuthController.localAuth);

router.post('/jwt-login', AuthController.jwtAuth);

// User
router.post('/users/all', UserController.getAllUsers);

router.post('/user', UserController.getUser);

router.post('/admin/user/edit', UserController.edit);

router.post('/admin/user/ban', UserController.ban);

router.post('/admin/user/unban', UserController.unban);

// Article
router.post('/article', ArticleController.getArticleById);

router.post('/article/name', ArticleController.getArticleByName);

router.post('/article/add', uploadToThumbnail.single('thumbnail'), ArticleController.create);

router.post('/article/add-watch', ArticleController.addWatch);

router.post('/article/edit', uploadToThumbnail.single('thumbnail'), ArticleController.edit);

router.post('/article/archive', ArticleController.archive);

router.post('/article/publish', ArticleController.publish);

router.post('/article/delete', ArticleController.remove);

router.post('/articles/all', ArticleController.getAllArticles);

router.post('/articles/category', ArticleController.getArticlesByCategory);

router.post('/articles/search', ArticleController.searchArticles);

// Images
router.post('/admin/image/add-content', uploadToContent.single('img'), ImageController.add);

router.post('/admin/image/delete', ImageController.remove);

// Pages
router.post('/page', PageController.getPage);

router.post('/page/add', PageController.create);

router.post('/page/edit', PageController.edit);

router.post('/page/delete', PageController.remove);

router.post('/page/all', PageController.getAllPages);

// Video
router.post('/video', VideoController.getVideoById);

router.post('/video/add', VideoController.create);

router.post('/video/edit', VideoController.edit);

router.post('/video/delete', VideoController.remove);

router.post('/video/all', VideoController.getAllVideos);

// News
router.post('/news/main', NewsController.getMainNews);

router.post('/news/videos', VideoController.getVideosForWidget);

router.post('/news/video', VideoController.getVideoByTitle);

router.post('/news/top', NewsController.getTop);

module.exports = router;
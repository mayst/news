const multer = require('multer');
const uuid = require('uuid');


const storageForContent = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/images/content')
    },
    filename: function (req, file, cb) {
        const ext = file.mimetype.split('/')[1];
        const name = uuid.v1() + '.' + ext;
        cb(null, name)
    }
});
const uploadToContent = multer({
    storage: storageForContent,
    fileFilter: function (req, file, callback) {
        const ext = file.mimetype.split('/')[1];
        if(['png', 'jpg', 'gif', 'jpeg'].indexOf(ext) === -1) {
            return callback(new Error('Only images are allowed'))
        }
        callback(null, true)
    }
});
module.exports = uploadToContent;

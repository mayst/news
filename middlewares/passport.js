const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const User = require('./../models/User');

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    async (email, password, done) => {
        try {
            const user = await User.findOne({ email: email });

            if (!user) {
                return done(null, false, { message: 'Incorrect email.' });
            }
            console.log(user.getUserInfo());
            const match = await user.validPassword(password, user.password);
            if (!match) {
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, user);
        } catch (err) {
            console.log(err);

            return done(null, false, { message: 'Invalid user' });
        }
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'my_secret'
    },
    async (jwtPayload, done) => {
        console.log('jwt strategy');
        try {
            let user = await User.findById(jwtPayload._id);

            return done(null, user);
        } catch (err) {
            console.log(err);

            return done(null, false, { message: 'Invalid token' });
        }
    }
));
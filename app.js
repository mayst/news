const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose'); // DB
const passport = require('passport'); // for Auth
const bluebird = require('bluebird'); // for promises
const v1 = require('./routes/v1');
const CONFIG = require('./config/config');

require('./middlewares/passport');

const app = express();

app.use(logger('dev'));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

// mongoDB
mongoose.Promise = bluebird;
const mongoLocation = CONFIG.db_dialect + '://' + CONFIG.db_host + ':' + CONFIG.db_port + '/' + CONFIG.db_name;
mongoose.connect(mongoLocation, { useNewUrlParser: true })
    .then(()=> { console.log(`Succesfully Connected to the Mongodb Database  at URL : mongodb://127.0.0.1/news`) })
    .catch(()=> { console.log(`Error Connecting to the Mongodb Database at URL : mongodb://127.0.0.1/news`) });

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    // res.header('Access-Control-Allow-Credentials', 'true');

    next();
});



// Route groups
app.use('/v1', v1);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err.message)
});

app.listen(3000);

module.exports = app;

import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Articles from '@/components/Articles'
import Article from '@/components/Article'
import Video from '@/components/Video'
import Search from '@/components/Search'
import Pages from '@/components/Pages'
import MainAdmin from '@/components/admin/Main'
import AdminUsers from '@/components/admin/user/Users'
import AdminEditUser from '@/components/admin/user/EditUser'
import AdminPages from '@/components/admin/page/Pages'
import AdminAddPage from '@/components/admin/page/AddPage'
import AdminEditPage from '@/components/admin/page/EditPage'
import AdminArticles from '@/components/admin/article/Articles'
import AdminAddArticle from '@/components/admin/article/AddArticle'
import AdminEditArticle from '@/components/admin/article/EditArticle'
import AdminVideos from '@/components/admin/video/Videos'
import AdminAddVideo from '@/components/admin/video/AddVideo'
import AdminEditVideo from '@/components/admin/video/EditVideo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    // Admin
    {
      path: '/admin',
      name: 'main-admin',
      component: MainAdmin
    },
    {
      path: '/admin/users/',
      name: 'admin-users',
      component: AdminUsers
    },
    {
      path: '/admin/user/:id/edit',
      name: 'admin-edit-user',
      component: AdminEditUser
    },
    {
      path: '/admin/pages/',
      name: 'admin-pages',
      component: AdminPages
    },
    {
      path: '/admin/add/page',
      name: 'admin-add-page',
      component: AdminAddPage
    },
    {
      path: '/admin/page/:url/edit',
      name: 'admin-edit-page',
      component: AdminEditPage
    },
    {
      path: '/admin/articles/',
      name: 'admin-articles',
      component: AdminArticles
    },
    {
      path: '/admin/add/article',
      name: 'admin-add-article',
      component: AdminAddArticle
    },
    {
      path: '/admin/article/:id/edit',
      name: 'admin-edit-article',
      component: AdminEditArticle
    },
    {
      path: '/admin/videos',
      name: 'admin-videos',
      component: AdminVideos
    },
    {
      path: '/admin/add/video',
      name: 'admin-add-video',
      component: AdminAddVideo
    },
    {
      path: '/admin/video/:id/edit',
      name: 'admin-edit-video',
      component: AdminEditVideo
    },
    // Client
    {
      path: '/',
      name: 'main',
      component: Main
    },
    /* {
      path: '/articles',
      name: 'articles',
      component: Articles
    }, */
    {
      path: '/articles/category/:category',
      name: 'articles-category',
      component: Articles
    },
    {
      path: '/article/:name',
      name: 'article',
      component: Article
    },
    {
      path: '/search/:info',
      name: 'search',
      component: Search
    },
    {
      path: '/:page',
      name: 'pages',
      component: Pages
    },
    {
      path: '/video/:name',
      name: 'video',
      component: Video
    }
  ]
})

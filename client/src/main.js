// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
require('moment/locale/ru')

Vue.config.productionTip = false

Vue.use(VueMoment, {
  moment
})

Vue.filter('striphtml', function (value) {
  const div = document.createElement('div')
  div.innerHTML = value
  return div.textContent || div.innerText || ''
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

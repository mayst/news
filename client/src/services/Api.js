import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: `http://localhost:3000/v1`,
    timeout: 10000,
    // withCredentials: true,
    responseType: 'json',
    // transformRequest: [(data) => JSON.stringify(data.data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}

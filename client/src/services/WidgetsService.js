import Api from '@/services/Api'
const axios = require('axios')

export default {
  getUSD (data) {
    return axios.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&date=' + data.date + '&json')
  },
  getEUR (data) {
    return axios.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=EUR&date=' + data.date + '&json')
  },
  getMainNews () {
    return Api().post('/news/main')
  },
  getVideos () {
    return Api().post('/news/videos')
  },
  getTopWatching () {
    return Api().post('/news/top')
  }
}

import Api from '@/services/Api'

export default {
  getUsers () {
    return Api().post('users/all')
  },
  getUser (id) {
    return Api().post('user', id)
  },
  editUser (params) {
    return Api().post('/admin/user/edit', params)
  },
  banUser (id) {
    return Api().post('admin/user/ban', id)
  },
  unbanUser (id) {
    return Api().post('admin/user/unban', id)
  },
  emailSub () {
    return Api().post('user/subscrb')
  }
}

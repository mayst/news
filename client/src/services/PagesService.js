import Api from '@/services/Api'

export default {
  getPage (url) {
    return Api().post('page', url)
  },
  getPages () {
    return Api().post('page/all')
  },
  addPage (params) {
    return Api().post('page/add', params)
  },
  editPage (params) {
    return Api().post('page/edit', params)
  },
  deletePage (id) {
    return Api().post('page/delete', id)
  }
}

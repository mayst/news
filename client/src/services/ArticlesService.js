import Api from '@/services/Api'

export default {
  getArticles (page) {
    return Api().post('articles/all', page)
  },
  addArticle (params) {
    return Api().post('article/add', params, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
  },
  getArticle (id) {
    return Api().post('article', id)
  },
  editArticle (params) {
    return Api().post('article/edit', params, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
  },
  archiveArticle (id) {
    return Api().post('article/archive', id)
  },
  publishArticle (id) {
    return Api().post('article/publish', id)
  },
  deleteArticle (id) {
    return Api().post('article/delete', id)
  },
  getArticleByName (name) {
    return Api().post('article/name', name)
  },
  getArticlesByCategory (category) {
    return Api().post('articles/category', category)
  },
  searchArticles (info) {
    return Api().post('articles/search', info)
  },
  addImage (img) {
    return Api().post('admin/image/add-content', img, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
  },
  addWatch (name) {
    return Api().post('article/add-watch', name)
  }
}

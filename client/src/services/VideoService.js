import Api from '@/services/Api'

export default {
  getVideos () {
    return Api().post('video/all')
  },
  async getVideo (id) {
    return Api().post('video', id)
  },
  async addVideo (params) {
    return Api().post('video/add', params)
  },
  async editVideo (params) {
    return Api().post('video/edit', params)
  },
  async getVideoByTitle (title) {
    return Api().post('news/video', title)
  }
}

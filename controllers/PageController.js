const Page = require('./../models/Page');


const create = async (req, res) => {
    try {
        const url = req.body.url;
        const name = req.body.name;
        const content = req.body.content;
        const page = await Page.findOne({ url: url });

        if(page) {
            return res.status(400).json({
                message: 'page with this url is already have'
            });
        }

        const newPage = new Page({
            url: url,
            name: name,
            content: content,
            created_at: Date.now(),
            updated_at: Date.now()
        });
        await newPage.save();

        return res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        return res.json({ status: 'fail' });
    }
};
module.exports.create = create;

const edit = async (req, res) => {
    try {
        const id = req.body.id;
        const url = req.body.url;
        const name = req.body.name;
        const content = req.body.content;

        await Page.findByIdAndUpdate(id, { url: url, name: name, content: content });

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ status: 'fail' });
    }
};
module.exports.edit = edit;

const remove = async (req, res) => {
    try {
        const id = req.body.id;
        console.log(id)
        await Page.findByIdAndDelete(id);

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ status: 'fail' });
    }
};
module.exports.remove = remove;

const getAllPages = async (req, res) => {
    try {
        const results = await Page.find({});
        res.json({ pages: results });
    } catch (err) {
        console.log(err);

        res.send(err);
    }
};
module.exports.getAllPages = getAllPages;

const getPage = async (req, res) => {
    try {
        const page = await Page.findOne({ url: req.body.url });
        res.json({ page: page });
    } catch (err) {
        console.log(err);

        return res.json({ error: 'can`t get page' });
    }
};
module.exports.getPage = getPage;
const Article = require('./../models/Article');
const CONFIG = require('./../config/config');


const create = async (req, res) => {
    try {
        const article = JSON.parse(req.body.article);
        const title = article.title;
        const content = article.content;
        const category = article.category;

        const newArticle = new Article();
        newArticle.title = title;
        newArticle.content = content;
        newArticle.category = category;
        if (req.file) { newArticle.thumbnail = req.file.filename }
        await newArticle.save();

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);
        res.json({ error: err });
    }
};
module.exports.create = create;

const edit = async (req, res) => {
    try {
        const article = JSON.parse(req.body.article);
        const id = article._id;
        const title = article.title;
        const content = article.content;
        const category = article.category;

        if (req.file) {
            await Article.findByIdAndUpdate(id, {
                title: title,
                content: content,
                category: category,
                thumbnail: req.file.filename,
                updated_at: Date.now()
            });
        } else {
            await Article.findByIdAndUpdate(id, {
                title: title,
                content: content,
                category: category,
                updated_at: Date.now()
            });
        }

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);
        res.json({ status: 'fail' });
    }
};
module.exports.edit = edit;

const addWatch = async (req, res) => {
    try {
        const title = req.body.name;

        await Article.update({ title: title }, { $inc: { watched: 1 } });
        res.sendStatus(200);
    } catch (e) {
        console.log(e);
        res.json({ status: 'error' });
    }
};
module.exports.addWatch = addWatch;

const archive = async (req, res) => {
    try {
        const id = req.body.id;

        const article = await Article.findByIdAndUpdate(id, { status: 'archived', updated_at: Date.now() });

        res.json({ article: article });
    } catch (err) {
        console.log(err);

        res.send(err);
    }
};
module.exports.archive = archive;

const publish = async (req, res) => {
    try {
        const id = req.body.id;

        const article = await Article.findByIdAndUpdate(id, { status: 'published', updated_at: Date.now() });

        res.json({ article: article });
    } catch (err) {
        console.log(err);

        res.send(err);
    }
};
module.exports.publish = publish;

const remove = async (req, res) => {
    try {
        const id = req.body.id;
        await Article.findByIdAndDelete(id);

        res.json({ message: 'article has been successfully deleted.'});
    } catch (err) {
        console.log(err);

        res.send(err);
    }
};
module.exports.remove = remove;

const getArticleById = async (req, res) => {
    try {
        const id = req.body.id;
        const article = await Article.findById(id, { thumbnail: 1, category: 1, title: 1, content: 1 });
        article.thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + article.thumbnail;

        res.json({ article: article });
    } catch (err) {
        console.log(err);

        res.send(err);
    }
};
module.exports.getArticleById = getArticleById;

const getArticleByName = async (req, res) => {
    try {
        const title = req.body.name;
        const article = await Article.findOne({ title: title });
        article.thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + article.thumbnail;

        res.json({ article: article });
    } catch (err) {
        console.log(err);

        res.json({ error: err });
    }
};
module.exports.getArticleByName = getArticleByName;

const getArticlesByCategory = async (req, res) => {
    try {
        const category = req.body.category;
        const articles = await Article.find({ category: category });
        for (let i = 0; i < articles.length; i++) {
            articles[i].thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + articles[i].thumbnail;
        }

        res.json({ articles: articles });
    } catch (e) {
        console.log(e);
        res.json({ e });
    }
};
module.exports.getArticlesByCategory = getArticlesByCategory;

const getAllArticles = async (req, res) => {
    try {
        let currPage = req.body.page;
        if (!currPage) { currPage = 1 }

        const responses = await Promise.all([
            Article.find().skip((currPage - 1) * 5).limit(5),
            Article.find().count()
        ]);

        const articles = responses[0];
        const countPages = Math.ceil(responses[1] / 5);

        res.json({ currPage: currPage, countPages: countPages, articles: articles, count: responses[1] });
    } catch (e) {
        console.log(e);
        res.json({ status: 'error' });
    }
};
module.exports.getAllArticles = getAllArticles;

const searchArticles = async (req, res) => {
    try {
        const name = req.body.info;
        const articles = await Article.find({ title: { $regex: name, $options: 'i' } });
        for (let i = 0; i < articles.length; i++) {
            articles[i].thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + articles[i].thumbnail;
        }

        res.json({ articles });
    } catch (e) {
        console.log(e)
        res.json({ e })
    }
};
module.exports.searchArticles = searchArticles;
const Article = require('./../models/Article');
const CONFIG = require('./../config/config');

const getMainNews = async (req, res) => {
    try {
        const news = await Article.find({ status: 'published' }, { status: 0, category: 0, __v: 0 }).limit(5);
        for (let i = 0; i < news.length; i++) {
            news[i].thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + news[i].thumbnail;
        }

        res.json({ news });
    } catch (err) {
        console.log(err)
        res.json({ error: err });
    }
};
module.exports.getMainNews = getMainNews;

const getTop = async (req, res) => {
    try {
        const news = await Article.find({ status: 'published' }).sort({ watched: -1 }).limit(2);
        for (let i = 0; i < news.length; i++) {
            news[i].thumbnail = CONFIG.conn_type + '://' + CONFIG.host + '/images/thumbnails/' + news[i].thumbnail;
        }

        res.json({ news });
    } catch (err) {
        console.log(err)
        res.json({ error: err });
    }
};
module.exports.getTop = getTop;
const fs = require('fs');
const path = require('path');
const CONFIG = require('./../config/config');


const add = (req, res) => {
    const path = CONFIG.conn_type + '://' + CONFIG.host + '/images/content/' + req.file.filename;
    return res.json({ url: path });
};
module.exports.add = add;

const remove = async (req, res) => {
    try {
        const name = req.body.name;
        const pathToFile = path.join(__dirname, '..', 'public', 'images', 'content', name);
        await fs.unlink(pathToFile);
    } catch (e) {
        console.log(e);
        return res.json({ status: 'error' })
    }
};
module.exports.remove = remove;
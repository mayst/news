const User = require('./../models/User');
const jwt = require('jsonwebtoken');


const create = async (req, res) => {
    try {
        const email = req.body.email;
        const password = req.body.password;

        const user = await User.findOne({ email: email });

        // if(!email) res.json({ msg: 'usr fail' });
        if(user) {
            return res.status(400).json({
                message: 'account with this mail is already have'
            });
        }

        const newUser = new User();
        newUser.email = email;
        newUser.password = await newUser.generateHash(password);
        await newUser.save();
        await req.login(newUser, {session: false});

        const token = await jwt.sign(newUser.toJSON(), 'my_secret', {
            expiresIn: 604800 // 1 week
        });

        return res.json({ newUser, token });
    } catch (err) {
        console.log(err);

        return res.status(400).json({ err });
    }
};
module.exports.create = create;

const edit = async (req, res) => {
    try {
        const id = req.body.id;
        const email = req.body.email;
        const role = req.body.role;

        await User.findByIdAndUpdate(id, { email: email, role: role });

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.edit = edit;

const ban = async (req, res) => {
    try {
        const id = req.body.id;
        await User.findByIdAndUpdate(id, { banned: true });

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.ban = ban;

const unban = async (req, res) => {
    try {
        const id = req.body.id;
        await User.findByIdAndUpdate(id, { banned: false });

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.unban = unban;

const getAllUsers = async (req, res) => {
    try {
        const results = await User.find({});
        res.json({ users: results });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }

/*    const timeout = ms => new Promise(res => setTimeout(res, ms));

    (async () => {
        try {
            console.log('start');
            const results = await timeout(10000);
            console.log('end');
            res.json({ success: 'Result after 5 sec', users: results });
        } catch (err) {
            throw err;
        }
    })();*/
};
module.exports.getAllUsers = getAllUsers;

const getUser = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        res.json({ user });
    } catch (err) {
        console.log(err);

        return res.json({ err });
    }
};
module.exports.getUser = getUser;
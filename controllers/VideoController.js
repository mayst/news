const Video = require('./../models/Video');


const create = async (req, res) => {
    try {
        const title = req.body.title;
        const url = req.body.url;

        const video = new Video({
            title: title,
            url: url,
            created_at: Date.now(),
            updated_at: Date.now()
        });
        await video.save();

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ error: err });
    }
};
module.exports.create = create;

const edit = async (req, res) => {
    try {
        const id = req.body.id;
        const title = req.body.title;
        const url = req.body.url;

        await Video.findByIdAndUpdate(id, {
            title: title,
            url: url,
            updated_at: Date.now()
        });

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ status: 'fail' });
    }
};
module.exports.edit = edit;

const remove = async (req, res) => {
    try {
        const id = req.body.id;
        await Video.findByIdAndDelete(id);

        res.json({ status: 'success' });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.remove = remove;

const getVideoById = async (req, res) => {
    try {
        const id = req.body.id;
        const video = await Video.findById(id);

        res.json({ video });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.getVideoById = getVideoById;

const getAllVideos = async (req, res) => {
    try {
        const videos = await Video.find();
        // const articles = await Article.paginate({}, { limit: 2 });

        res.json({ videos });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.getAllVideos = getAllVideos;

const getVideosForWidget = async (req, res) => {
    try {
        const videos = await Video.find().limit(6);
        // const articles = await Article.paginate({}, { limit: 2 });

        res.json({ videos });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.getVideosForWidget = getVideosForWidget;

const getVideoByTitle = async (req, res) => {
    try {
        const title = req.body.title;
        const video = await Video.findOne({ title: title });

        res.json({ video });
    } catch (err) {
        console.log(err);

        res.json({ err });
    }
};
module.exports.getVideoByTitle = getVideoByTitle;
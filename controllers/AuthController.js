const jwt = require('jsonwebtoken');
const passport = require('passport');


const localAuth = async (req, res) => {
    try {
        await passport.authenticate('local', { session: false }, async (err, user, message) => {
            try {
                if (!user) {
                    return res.status(400).json({
                        message: message,
                        err: err
                    });
                }
                /*await req.login(user, {session: false}, async () => {
                    try {
                        // generate a signed son web token with the contents of user object and return it in the response
                        let token = await jwt.sign(user.toJSON(), 'my_secret', {
                            expiresIn: 604800 // 1 week
                        });

                        return res.json({ user, token });
                    } catch (err) {
                        console.log(err);

                        return res.send(err);
                    }
                });*/
                await req.login(user, {session: false});
                let token = await jwt.sign(user.toJSON(), 'my_secret', {
                    expiresIn: 604800 // 1 week
                });

                return res.json({ user, token });
            } catch (err) {
                console.log(err);

                return res.send(err);
            }
        })(req, res);
    } catch (err) {
        console.log(err);

        return res.send(err);
    }
};
module.exports.localAuth = localAuth;

const jwtAuth = async function(req, res) {
    try {
        await passport.authenticate('jwt', (err, user, message) => {
            if (!user) {
                return res.status(400).json({
                    err: err,
                    user: user,
                    message: message
                });
            }

            res.json({ success: 'You are authenticated with JWT!', user: user })
        })(req, res);
    } catch (err) {
        console.log(err);

        return res.send(err);
    }
};
module.exports.jwtAuth = jwtAuth;